﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="subida.aspx.cs" Inherits="photogit.subida" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                Imagen agregada:
                <br />
                <asp:Image ID="imgP" Width="200" ImageUrl="https://img.blogs.es/iahuawei/wp-content/uploads/2018/12/kirin.jpg" runat="server"/>
                <br />
                <br />
                Archivo:
                <asp:FileUpload ID="fUploadImagen" accept=".jpg" runat="server" CssClass="form-control" />
                <br />
                Titulo de imagen:
                <asp:TextBox ID="txtTitulo" runat="server" CssClass="form-control"></asp:TextBox>
                <br />
                <asp:Button ID="btnSubir" runat="server" Text="Button" CssClass="btn btn-success" OnClick="btnSubir_Click"/> 
            </div>
        </div>
        <div class="row">
            <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                    <div>
                        <img class="img-responsive" width="200" src="data:image/jpg;base64,<%#  Convert.ToBase64String((byte[])DataBinder.Eval(Container.DataItem,"imagen")) %>" /> 
                        <%# DataBinder.Eval(Container.DataItem,"nombre") %>
                        <br />
                    </div>

                </ItemTemplate>
            </asp:Repeater>

        </div>
    </div>
    </form>
</body>
</html>
