﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="photogit.Login" %>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login PhotoGit</title>
    <link rel="stylesheet" href="css/estilos.css">
  </head>
  <body>
    <div class="contenedor-form">
      <div class="titulo">
        <h1>PhotoGit</h1>
      </div>
      <div class="toggle">
        <span>Crear Cuenta</span>
      </div>
      <div class="formulario">
        <h2>Iniciar sesión</h2>
        <form class="#">
          <input type="text" placeholder="Usuario" required>
          <input type="password" placeholder="Contraseña" required>
          <input type="submit" value="Iniciar sesión">
        </form>
      </div>

      <div class="formulario">
        <h2>Crear Cuenta</h2>
        <form class="#">
          <input type="text" placeholder="Usuario" required>
          <input type="password" placeholder="Contraseña" required>
          <input type="email" placeholder="Correo electronico" required>
          <input type="text" placeholder="Telefono" required>
          <input type="submit" value="Registrarse">
        </form>
      </div>

      <div class="reset-password">
        <a href="#">olvidé mi Contraseña</a>
      </div>
    </div>
    <script src="jquery/jquery-3.4.1.min.js"></script>
    <script src="jquery/main.js"></script>
  </body>
</html>
