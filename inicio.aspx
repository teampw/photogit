﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inicio.aspx.cs" Inherits="photogit.inicio" %>

<!DOCTYPE html>

<html lang="es" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>PhotoGit</title>
    <link rel="stylesheet" href="css/inicio.css" type="text/css"/>
    </head>
<body style="background-image:url(ima/photo.jpg)">
    <form id="Form1" runat="server">
        <div>
            
                
            <asp:ImageButton CssClass="img-responsive" ID="logotipo" runat="server" ImageUrl="~/ima/logoP.png" padding="10px 10px 20px 10px" margin="10px 20px" Width="120px" Height="26px" />
            <asp:TextBox ID="TextBox1" runat="server" Width="442px" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            &nbsp;<asp:ImageButton ID="ImageButton2" runat="server" Width="25px" ImageUrl="~/ima/lupa.png" OnClick="ImageButton2_Click" />
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/ima/usuario.png" runat="server" Width="33px" OnClick="ImageButton1_Click" />
                
        </div>
    <br />
        <div style="width:100%;">
            <div style="width:100%; text-align:center"> <h2>AQUI APARECEN SUS PROYECTOS</h2></div>
            <div  style="width:100%; display:inline-block; text-align:center;">
                
                <asp:Repeater ID="repetidor" runat="server" OnItemCommand="repetidor_ItemCommand">
                    <ItemTemplate>
                        <div style="width:19%; display:inline-block;">
                            <asp:ImageButton style="width:90%;text-align:center;" ImageUrl="~/ima/carpeta.png" ID="carpeta" runat="server"  CommandName="link"  Enabled="true" PostBackUrl="~/carpeta.aspx"/>
                        <div style="width:90%; display:inline-block; text-align:center" class="nombre" > <%# DataBinder.Eval(Container.DataItem,"nombre")%></div> 
                        </div>
                     </ItemTemplate>

                </asp:Repeater>
            </div>
        </div>
        <div>
        </div>
    
    <div>
        <br />
    </div>
        </form>
</body>
</html>
