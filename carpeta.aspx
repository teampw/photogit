﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="carpeta.aspx.cs" Inherits="photogit.carpeta" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Imagenes</title>
</head>
<body style="background-image:url(ima/photo.jpg)">
    <form id="form1" runat="server">
    <div>
            
                
            <asp:ImageButton CssClass="img-responsive" ID="logotipo" runat="server" ImageUrl="~/ima/logoP.png" padding="10px 10px 20px 10px" margin="10px 20px" Width="120px" Height="26px" /><asp:TextBox ID="TextBox1" runat="server" Width="442px"></asp:TextBox>
            &nbsp;<asp:ImageButton ID="ImageButton2" runat="server" Width="25px" ImageUrl="~/ima/lupa.png" OnClick="ImageButton2_Click" />
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/ima/usuario.png" runat="server" Width="33px" />
                
        </div>
    <br />
        <div style="width:100%;">
            <div style="width:100%; text-align:center"> <h2>"PROYECTO"</h2></div>
            <div  style="width:100%; display:inline-block; text-align:center;">
                
                <asp:Repeater ID="repetidor" runat="server" OnItemCommand="repetidor_ItemCommand">
                    <ItemTemplate>
                        <div style="width:auto; display:inline-block;">
                        <img style="height:250px; display:inline-flexbox;" src="data:image/jpg;base64,<%#  Convert.ToBase64String((byte[])DataBinder.Eval(Container.DataItem,"imagen")) %>" />
                        <div style="width:90%; display:inline-block; text-align:center" class="nombre" > <%# DataBinder.Eval(Container.DataItem,"nombre")%></div> 
                        </div>
                     </ItemTemplate>

                </asp:Repeater>
                    
            </div>
        </div>
        <div>
        </div>
    
    <div>
        <br />
    </div>
    </form>
</body>
</html>
