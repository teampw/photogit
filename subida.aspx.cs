﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;

namespace photogit
{
    public partial class subida : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ConsultarImagen();
        }

        protected void btnSubir_Click(object sender, EventArgs e)
        {
            string conexion = "Data Source = HEBERS\\SQLEXPRESS; Initial Catalog =PhotoGitB; User = HEBERS\\toshiba; Integrated Security=True";

            //Obtener datos de la imagen
            int tamanio = fUploadImagen.PostedFile.ContentLength;
            byte[] ImagenOriginal = new byte[tamanio];
            fUploadImagen.PostedFile.InputStream.Read(ImagenOriginal,0,tamanio);
            Bitmap ImagenOriginalBinaria = new Bitmap(fUploadImagen.PostedFile.InputStream);

            //Crear imagen Thumbnail
            System.Drawing.Image imtThumbnail;
            int tamanioThumbnail = 200;
            imtThumbnail = RedimencionarImagen(ImagenOriginalBinaria,tamanioThumbnail);
            byte[] bImagenThumbnail = new byte[tamanioThumbnail];
            ImageConverter Convertidor = new ImageConverter();
            bImagenThumbnail = (byte[])Convertidor.ConvertTo(imtThumbnail,typeof(byte[]));

            //Insertar en la base de datos
            SqlConnection conexionSQL = new SqlConnection(conexion);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO Imagen(nombre,imagen,id_portafolio,likes,unlikes) VALUES(@nombre,@imagen,@portafolio,0,0)";
            cmd.Parameters.Add("@imagen", SqlDbType.Image).Value = bImagenThumbnail;//ImagenOriginal;
            cmd.Parameters.Add("@nombre",SqlDbType.Text).Value=txtTitulo.Text;
            cmd.Parameters.Add("@portafolio",SqlDbType.Int).Value=5;
            cmd.CommandType= CommandType.Text;
            cmd.Connection =conexionSQL;
            conexionSQL.Open();
            cmd.ExecuteNonQuery();
            string ImagenDataURL64 = "data:image/jpg;base64," + Convert.ToBase64String(bImagenThumbnail);//ImagenOriginal);
            imgP.ImageUrl = ImagenDataURL64;
            ConsultarImagen();

        }

        public System.Drawing.Image RedimencionarImagen(System.Drawing.Image Original, int Alto)
        {
            var Radio= (double)Alto/Original.Height;
            var NuevoAncho= (int)(Original.Width * Radio);
            var NuevoAlto= (int)(Original.Height * Radio);
            var NuevaImagenRedimencionada= new Bitmap(NuevoAncho,NuevoAlto);
            var g=Graphics.FromImage(NuevaImagenRedimencionada);
            g.DrawImage(Original,0,0,NuevoAncho,NuevoAlto);
            return NuevaImagenRedimencionada;
        }

        protected void ConsultarImagen()
        {
            string conexion = "Data Source = HEBERS\\SQLEXPRESS; Initial Catalog =PhotoGitB; User = HEBERS\\toshiba; Integrated Security=True";
            SqlConnection conexionSQL = new SqlConnection(conexion);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT imagen,nombre FROM Imagen ORDER BY id_imagen";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conexionSQL;
            conexionSQL.Open();

            DataTable ImagenesBD = new DataTable();
            ImagenesBD.Load(cmd.ExecuteReader());
            Repeater1.DataSource = ImagenesBD;
            Repeater1.DataBind();
            conexionSQL.Close();



        }
    }
}